const fs = require('fs');
const bodyParser = require('body-parser');
const jsonServer = require('json-server');
const jwt = require('jsonwebtoken');
const path = require("path");


const server = jsonServer.create()
const router = jsonServer.router(path.resolve(__dirname, './db.json'))
const userdb = JSON.parse(fs.readFileSync(path.resolve(__dirname, './users.json'), 'UTF-8'))

server.use(bodyParser.urlencoded({extended: true}))
server.use(bodyParser.json())
server.use(jsonServer.defaults());

const SECRET_KEY = '123456789'

const expiresIn = '30d'

// Create a token from a payload 
const createToken = (payload) =>{
  return jwt.sign(payload, SECRET_KEY, {expiresIn})
}

// Verify the token 
const verifyToken = (token) =>{
  return  jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ?  decode : err)
}

// Check if the user exists in 'database'
const isAuthenticated = ({email, password}) =>{
  return userdb.users.findIndex(user => user.email === email && user.password === password) !== -1
}

// Register New User
server.post('/auth/register', (req, res) => {
    const {email, password} = req.body;

    if(isAuthenticated({email, password}) === true) {
        const status = 401;
        const message = 'User already exists';
        res.status(status).json({status, message});
        return
    }

    fs.readFile(path.resolve(__dirname,'./users.json'), (err, data) => {  
        if (err) {
            const status = 401
            const message = err
            res.status(status).json({status, message})
            return
        };

        const fetchedUsers = JSON.parse(data.toString());
        const lastItemId = fetchedUsers.users[fetchedUsers.users.length-1].id;
        const user = fetchedUsers.users.find(user => user.email == email);
        if (user && Object.entries(user).length > 0) {
            const status = 401;
            const message = 'User already exists';
            res.status(status).json({status, message});
            return
        }
        fetchedUsers.users.push({id: lastItemId + 1, email: email, password: password}); 
        fs.writeFile(path.resolve(__dirname,'./users.json'), JSON.stringify(fetchedUsers), (err, res) => {
            if (err) {
            const status = 401
            const message = err
            res.status(status).json({status, message})
            return
            }
        });
    });

    const accessToken = createToken({email, password})
    res.status(200).json({email, accessToken})
})

server.post('/auth/login', (req, res) => {
    const {email, password} = req.body;
    if (isAuthenticated({email, password}) === false) {
        const status = 401
        const message = 'Incorrect email or password'
        res.status(status).json({status, message})
        return
    }
    const accessToken = createToken({email, password})
    res.status(200).json({email, accessToken})
})

server.use(/^(?!\/auth).*$/,  (req, res, next) => {
    if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
        const status = 403
        const message = 'Error in authorization format'
        res.status(status).json({status, message})
        return
    }
    try {
        let verifyTokenResult;
        verifyTokenResult = verifyToken(req.headers.authorization.split(' ')[1]);
        if (verifyTokenResult instanceof Error) {
            const status = 403;
            const message = 'Access token not provided';
            res.status(status).json({status, message})
            return
        }
        next()
    } catch (err) {
        const status = 403;
        const message = 'Error accessToken is revoked';
        res.status(status).json({status, message})
    }
})

server.use(router)

server.listen(8000)