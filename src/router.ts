import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Dashboard from './components/Dashboard.vue';
import AccountList from './components/AccountList.vue';
import AccountClaimsList from './components/AccountClaimsList.vue';
import Login from './components/Login.vue';
import Register from './components/Register.vue';
import store from './store';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
      hideForAuth: true
    }
  },
  {
    path: "/register",
    name: "register",
    component: Register,
    meta: {
      hideForAuth: true
    }
  },
  {
    path: "/",
    name: "dashboard",
    component: Dashboard,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/accounts",
    name: "accounts",
    component: AccountList,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/accounts/:id",
    name: "account-claims",
    props: true,
    component: AccountClaimsList,
    meta: {
      requiresAuth: true
    }
  }

];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const loggedIn = store.getters['auth/isLoggedIn'];
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (loggedIn) {
      next()
    } else {
      router.push({
          path: '/login',
          params: { nextUrl: to.fullPath }
      })
    }
  } else if(to.matched.some(record => record.meta.hideForAuth)) {
    if (loggedIn) {
      router.push({
          path: '/',
          params: { nextUrl: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router;