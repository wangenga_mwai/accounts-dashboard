class NumberFormatter {
    numberToDecimal(num: number) {
        return + (num / 100).toFixed(2);
    }
    thousandsSeparator(num: { toString: () => string; })
    {
      var num_parts = num.toString().split(".");
      num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      return num_parts.join(".");
    }
    formatToCurrency(num: number, toDecimal: boolean = true) {
        const formattedAmount = this.thousandsSeparator(toDecimal ? this.numberToDecimal(num) : num);
        return `€ ${formattedAmount}`;
    }

}

export default new NumberFormatter;