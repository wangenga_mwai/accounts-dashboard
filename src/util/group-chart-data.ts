import numberFormatter from "./number-formatter";

class GroupChartData {
    groupByMonthYear(claims: Array<object>, year: string) {
        const groupedDueData = Array.from({length:12},()=> (
          {due:0,pastDue:0,cummulativePastDue:0,onTimePayment:0,pastDuePayment:0,totalPayment:0,cummulativePayment:0}))
        let cummulativePastDue: Array<number> = [];
        let cummulativePayment: Array<number> = [];
        let totalClaims: number = 0;
    
        claims.forEach((item: any) => {
          const monthYear = item.dueDate.split('-');
          const currentYear = parseInt(year);
          const yearDue = parseInt(monthYear[0]);
          const monthDue = parseInt(monthYear[1]);
          const dueMonthIndex = monthDue - 1;
          const dueMonthData = groupedDueData[dueMonthIndex];
          const dueDate = new Date(item.dueDate);
          const dateToday = new Date();      
          const fees = numberFormatter.numberToDecimal(item.fees);
            
          if (yearDue != currentYear) return;
    
          groupedDueData[dueMonthIndex].due = dueMonthData.due + fees;
          totalClaims += fees
           
          if (!item.paidAt && dueDate < dateToday) {
            cummulativePastDue[dueMonthIndex] = dueMonthData.pastDue + fees
            groupedDueData[dueMonthIndex].pastDue = cummulativePastDue[dueMonthIndex];
            groupedDueData[dueMonthIndex].cummulativePastDue = cummulativePastDue.slice(0,dueMonthIndex + 1).reduce((a, b)=>a+b);
          } else {
            const paidMonthIndex = parseInt(item.paidAt.split('-')[1]) - 1;
            const paidMonthData = groupedDueData[paidMonthIndex];
    
            cummulativePayment[paidMonthIndex] = paidMonthData.totalPayment + fees
            groupedDueData[paidMonthIndex].totalPayment = paidMonthData.totalPayment + fees
            groupedDueData[paidMonthIndex].cummulativePayment = cummulativePayment.slice(0,paidMonthIndex + 1).reduce((a, b)=>a+b);
            
            if (new Date(item.paidAt) <= dueDate) {
              groupedDueData[paidMonthIndex].onTimePayment = paidMonthData.onTimePayment + fees
            } else {
              groupedDueData[paidMonthIndex].pastDuePayment = paidMonthData.pastDuePayment + fees
            }
          }
        });
        
        return {groupedDueData, cummulativePastDue, cummulativePayment, totalClaims};
      }
}

export default new GroupChartData;