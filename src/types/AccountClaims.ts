export default interface AccountClaims {
    data: [
        {
            id: string,
            accountId: string,
            currency: string,
            baseAmount: number,
            fees: number,
            dueDate: string,
            status: string,
            paidAt?: number
        }
    ]
    total: number
}