import http from "../http-common";

class AuthService {
  async login(user: { email: string; password: string; }) {
    const response = await http
        .post('/auth/login', {
            email: user.email,
            password: user.password
        });
    if (response.data.access_token) {
        localStorage.setItem('user', JSON.stringify(response.data));
    }
    return response.data;
  }

  logout() {
    localStorage.removeItem('user');
  }

  async register(user: { email: string; password: string; }) {
    const response = await http
        .post('/auth/register', {
            email: user.email,
            password: user.password
        });
    if (response.data.access_token) {
        localStorage.setItem('user', JSON.stringify(response.data));
    }
    return response.data;
  }
}

export default new AuthService();