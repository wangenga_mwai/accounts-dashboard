import http from "../http-common";
import authHeader from './auth-header';

class AccountsDataService {
  getAllAccounts() {
    return http.get(`/accounts?_sort=debtor.firstName&_order=asc`, { headers: authHeader() });
  }
  getAccount(id: string) {
    return http.get(`/accounts/${id}`, { headers: authHeader() }) 
  }
  getAccountClaims(params: any) {
    const status = params.status ? `status=${params.status}&` : '';
    return http.get(`/accounts/${params.id}/claims?${status}&status_ne=DELETED&_sort=dueDate&_order=asc`, { headers: authHeader() });
  }
  searchBy(params: any) {
    return http.get(`/accounts?q=${params.query}`, { headers: authHeader() });
  }
}

export default new AccountsDataService();
