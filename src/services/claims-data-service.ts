import http from "../http-common";
import authHeader from './auth-header';

class ClaimsDataService {
    fetchClaims() {
        return http.get(`/claims?status_ne=DELETED&_sort=paidAt&_order=asc`, { headers: authHeader() });
    }
}
export default new ClaimsDataService();