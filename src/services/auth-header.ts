import store from '@/store';

export default function authHeader() {
    const user = store.getters['auth/getUser']
    if (user && user.accessToken) {
        return { Authorization: 'Bearer ' + user.accessToken };
    } else {
        return {};
    }
}