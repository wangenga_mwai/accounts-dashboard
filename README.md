# Code Challenge
## Start!
A couple things to note:

- API is now behind an auth guard & listening on port 8000 (wanted to preserve 9001 in case you need it)
- You can find test login credentials under `api/users.json`
- You can register a new user as long as the app has write permissions


```
npm install
npm run challenge
npm run test:unit (run unit tests)
```

The following endpoints are available
```
(Public)
http://localhost:9001/accounts
http://localhost:9001/claims
(Protected)
http://localhost:8000/accounts
http://localhost:8000/claims
```
