import { createLocalVue, mount } from '@vue/test-utils';
import Vuex from 'vuex';
import ElementUI from 'element-ui';
import App from '@/App.vue';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

const store = new Vuex.Store({
    state: {
        auth: {
            status: {
                loggedIn: true
            },
            user: {
                accessToken:'someTestToken',
                email: 'test1@email.com'
            }
        }
    }
})
  
describe('App.vue', () => {
    it('renders as expected', async () => {
        const mockStore = { 
            dispatch: jest.fn().mockImplementation(() => Promise.resolve()),

        }
        const wrapper = mount(App, {
            localVue,
            stubs: ['router-link', 'router-view'],
            mocks: {
                $store: mockStore 
            },
            store
        })
        expect(wrapper.find('.el-dropdown-menu__item').text()).toBe('Logout')
    })
    it('Logout works as expected', async () => {
        const mockStore = { 
            dispatch: jest.fn().mockImplementation(() => Promise.resolve()),
            state: {
                auth: {
                    status: {
                        loggedIn: true
                    },
                    user: {
                        accessToken:'someTestToken',
                        email: 'test1@email.com'
                    }
                }
            }
        }
        const wrapper = mount(App, {
            localVue,
            stubs: ['router-link', 'router-view'],
            mocks: {
              $store: mockStore 
            },
        })
        const dropdown = wrapper.find('.el-dropdown-menu__item');
        await dropdown.trigger('click');
        expect(mockStore.dispatch).toHaveBeenCalledWith('auth/logout')
    })
})
