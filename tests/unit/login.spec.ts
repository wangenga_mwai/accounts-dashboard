import { createLocalVue, mount } from '@vue/test-utils';
import Vuex from 'vuex';
import ElementUI from 'element-ui';
import Login from '@/components/Login.vue';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)

const mockStore = { 
  dispatch: jest.fn().mockImplementation(() => Promise.resolve()),
  state: {
      auth: {
          status: {
              loggedIn: false
          },
          user: null
      }
  }
}

describe('Login.vue', () => {
  it('renders as expected', () => {
    const wrapper = mount(Login, {
      localVue,
      stubs: ['router-link'],
      mocks: {
        $store: mockStore 
      }
    })
    expect(wrapper.find('.el-card__header h1').text()).toBe('Sign in');
    expect(wrapper.find('button').text()).toBe('Login');
  })

  it('triggers validation', async () => {
    const wrapper = mount(Login, {
      localVue,
      stubs: ['router-link'],
      mocks: {
        $store: mockStore 
      }
    })
    const button = wrapper.find('button');
    await button.trigger('click');
    expect(wrapper.find('.is-error').text()).toMatch('please input a valid email')
  })

  it('allows for login if validation is true', async () => {
    const wrapper = mount(Login, {
      localVue,
      stubs: ['router-link'],
      mocks: {
        $store: mockStore 
      }
    })
    wrapper.findAll('input').at(0).setValue('test1@email.com')
    wrapper.findAll('input').at(1).setValue('test1')
    const button = wrapper.find('button');
    await button.trigger('click');
    expect(mockStore.dispatch).toHaveBeenCalledWith(
      'auth/login',
      { email: 'test1@email.com', password: 'test1' }
    )
  })
})
