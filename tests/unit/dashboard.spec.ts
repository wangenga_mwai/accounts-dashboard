import { createLocalVue, mount } from '@vue/test-utils';
import Vuex from 'vuex';
import ElementUI from 'element-ui';
import Dashboard from '@/components/Dashboard.vue';

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(ElementUI)
  
describe('Dashboard.vue', () => {
    it('renders as expected', async () => {
        const mockStore = { 
            dispatch: jest.fn().mockImplementation(() => Promise.resolve()),
            state: {
                auth: {
                    status: {
                        loggedIn: true
                    },
                    user: {
                        accessToken:'someTestToken',
                        email: 'test1@email.com'
                    }
                }
            }
        }
        const wrapper = mount(Dashboard, {
            localVue,
            stubs: ['router-link'],
            mocks: {
                $store: mockStore 
            }
        })
        expect(wrapper.findAll('.el-card').at(0).text()).toMatch('Claims Summary')
        expect(wrapper.findAll('.el-card').at(1).text()).toMatch('Payments Summary')
        expect(wrapper.findAll('.el-card').at(2).text()).toMatch('Period Summary')
    })
})
